import { Region } from './region';

export class Cliente {
  public id: number;
  public nombre: string;
  public apellido: string;
  public createAt: string;
  public email: string;
  public foto: string;
  public region: Region;
  constructor() {
    this.id = 0;
    this.nombre = '';
    this.apellido = '';
    this.createAt = '';
    this.email = '';
    this.foto = '';
    this.region = new Region();
  }
}
