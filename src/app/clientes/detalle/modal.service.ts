import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  modal: boolean = false;

  private notificarSubida = new EventEmitter<any>();
  constructor() {}

  getNotificadorSubida() {
    return this.notificarSubida;
  }

  abrirModal() {
    this.modal = true;
  }

  cerrarModal() {
    this.modal = false;
  }
}
