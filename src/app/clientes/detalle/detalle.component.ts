import { Component, Input, OnInit } from '@angular/core';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { ModalService } from './modal.service';

@Component({
  selector: 'detalle-cliente',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],
})
export class DetalleComponent implements OnInit {
  @Input() cliente: Cliente = new Cliente();
  titulo: string = 'Detalles del cliente: ';
  fotoSeleccionada: File | undefined;

  progreso: number = 0;
  constructor(
    private service: ClienteService,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {}

  getModalService(): ModalService {
    return this.modalService;
  }

  seleccionarFoto(event: any) {
    this.fotoSeleccionada = event.target.files[0];
    this.progreso = 0;
    console.log(this.fotoSeleccionada);
    // @ts-ignore
    if (this.fotoSeleccionada?.type.indexOf('image') < 0) {
      swal.fire(
        'Error con el archivo selccionado',
        `El archivo no es una foto o el formato no esta soportado`,
        'error'
      );
      // @ts-ignore
      this.fotoSeleccionada = null;
    }
  }

  subirFoto(): void {
    if (this.fotoSeleccionada) {
      this.service
        .subirFoto(this.fotoSeleccionada, this.cliente?.id + '')
        .subscribe((event) => {
          if (event.type === HttpEventType.UploadProgress) {
            // @ts-ignore
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            const response: any = event.body;
            this.cliente = response.Cliente as Cliente;
            this.modalService.getNotificadorSubida().emit(this.cliente);
            console.log(response);
            swal.fire({
              title: 'Foto subida correctamente',
              text: `El cliente ha sido actualizado con la foto seleccionada: ${response.Mensaje}`,
              icon: 'success',
            });
          }
        });
    } else {
      swal.fire({
        title: 'Elija una imagen primero',
        text: 'Antes debe seleccionar la foto que desea cargar',
        icon: 'error',
      });
    }
  }

  cerrarModal() {
    this.modalService.cerrarModal();
    this.fotoSeleccionada = undefined;
    this.progreso = 0;
  }
}
