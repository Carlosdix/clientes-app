import { Injectable } from '@angular/core';
import { Cliente } from './cliente';
import { formatDate, DatePipe } from '@angular/common';
import { Observable, throwError } from 'rxjs';
import {
  HttpClient,
  HttpEvent,
  HttpHeaders,
  HttpRequest,
} from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import {Region} from './region';

@Injectable({
  providedIn: 'root',
})
export class ClienteService {
  private urlEndpoint: string = 'http://localhost:8080/api/clientes/';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private router: Router) {}

  getRegiones():Observable<Region[]>{
    return this.http.get<Region[]>(this.urlEndpoint+'regiones')
  }

  getClientes(page: number): Observable<any> {
    return this.http.get(this.urlEndpoint + `page/${page}`).pipe(
      tap((response: any) => {
        (response.content as Cliente[]).forEach((cliente) => {
          console.log(cliente.nombre);
        });
      }),
      map((response: any) => {
        (response.content as Cliente[]).map((cliente) => {
          cliente.nombre = cliente.nombre.toUpperCase();
          return cliente;
        });
        return response;
      }),
      tap((response) => {
        (response.content as Cliente[]).forEach((cliente) => {
          console.log(cliente.nombre);
        });
      })
    );
  }

  create(pCliente: Cliente): Observable<Cliente> {
    return this.http
      .post(this.urlEndpoint, pCliente, {
        headers: this.httpHeaders,
      })
      .pipe(
        map((response: any) => response.Cliente as Cliente),
        catchError((e) => {
          if (e.status == 400) {
            return throwError(e);
          }
          swal.fire({
            title: `${e.error.Mensaje}`,
            text: `${e.error.Error}`,
            icon: 'error',
          });
          console.log(e.error);
          return throwError(e);
        })
      );
  }

  getCliente(id: string): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.urlEndpoint}${id}`).pipe(
      catchError((e) => {
        this.router.navigate(['/clientes']);
        swal.fire({
          title: 'Problema al cargar el cliente',
          text: `Error inesperado: ${e.error.Mensaje}`,
          icon: 'error',
        });
        console.log(e.error);
        return throwError(e);
      })
    );
  }

  update(cliente: Cliente): Observable<any> {
    return this.http
      .put<any>(`${this.urlEndpoint}${cliente.id}`, cliente, {
        headers: this.httpHeaders,
      })
      .pipe(
        catchError((e) => {
          if (e.status == 400) {
            return throwError(e);
          }
          swal.fire({
            title: 'Error al editar al cliente',
            text: `${e.error.Mensaje}`,
            icon: 'error',
          });
          console.log(e.error);
          return throwError(e);
        })
      );
  }

  delete(id: number): Observable<Cliente> {
    return this.http
      .delete<Cliente>(`${this.urlEndpoint}${id}`, {
        headers: this.httpHeaders,
      })
      .pipe(
        catchError((e) => {
          swal.fire({
            title: 'Error al intentar eliminar al cliente',
            text: `${e.error.Mensaje}`,
            icon: 'error',
          });
          console.log(e.error);
          return throwError(e);
        })
      );
  }

  subirFoto(archivo: File, id: string): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    const res = new HttpRequest(
      'POST',
      `${this.urlEndpoint}upload/`,
      formData,
      {
        reportProgress: true,
      }
    );
    formData.append('archivo', archivo);
    formData.append('id', id);
    return this.http.request(res);
  }
}
