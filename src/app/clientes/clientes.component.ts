import { Component, Input, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import swal from 'sweetalert2';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { ModalService } from './detalle/modal.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
})
export class ClientesComponent implements OnInit {
  clientes: Cliente[] = [];
  paginador: any;
  clienteSeleccionado: Cliente = new Cliente();

  constructor(
    private clientService: ClienteService,
    private modalService: ModalService,
    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activateRoute.paramMap.subscribe((params) => {
      let page: number = Number(params.get('page'));
      if (!page) {
        page = 0;
      }
      this.clientService
        .getClientes(page)
        .pipe(
          tap((response) => {
            console.log('ClientesComponent: tap3');
            (response.content as Cliente[]).forEach((cliente) => {
              console.log(cliente.nombre);
            });
          })
        )
        .subscribe((response) => {
          this.clientes = response.content as Cliente[];
          this.paginador = response;
        });
    });
    // @ts-ignore
    this.modalService.getNotificadorSubida().subscribe((cliente) => {
      this.clientes = this.clientes.map((clienteOriginal) => {
        if (cliente.id === clienteOriginal.id) {
          clienteOriginal.foto = cliente.foto;
        }
        return clienteOriginal;
      });
    });
  }

  delete(pCliente: Cliente): void {
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: '¿Estas segurode eliminar a este cliente?',
        text: `Estas apunto de eliminar a ${
          pCliente.nombre + ' ' + pCliente.apellido
        }`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, borrar clientet!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.clientService.delete(pCliente.id).subscribe((response) => {
            this.clientes = this.clientes.filter((cli) => cli !== pCliente);
            swalWithBootstrapButtons.fire(
              'Eliminado!',
              'El cliente ha sido eliminado con exito',
              'success'
            );
          });
        }
      });
  }

  abrirModal(cliente: Cliente) {
    this.clienteSeleccionado = cliente;
    this.modalService.abrirModal();
  }
}
