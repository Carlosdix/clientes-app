import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Region } from './region';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {
  cliente: Cliente = new Cliente();
  titulo: string = 'Formulario para creacion de clientes';
  errores: string[] = [];
  regiones: Region[] = [];
  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private activatedRouted: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarCliente();
    this.clienteService
      .getRegiones()
      .subscribe((regiones) => (this.regiones = regiones));
  }

  datosCorrectos(): void {
    let list = '';
    this.errores.forEach(function (value) {
      list = `<h6>${list.concat('-').concat(value)}</h6><br>`;
    });
    swal.fire(
      'Datos incorrectos',
      `Verifica y revisalos nuevamente<br><br>${list}`,
      'error'
    );
  }

  cargarCliente(): void {
    this.activatedRouted.params.subscribe((params) => {
      let id = params['id'];
      if (id) {
        this.clienteService
          .getCliente(id)
          .subscribe((cliente) => (this.cliente = cliente));
      }
    });
  }

  create(): void {
    console.log(this.cliente);
    this.clienteService.create(this.cliente).subscribe(
      (cliente) => {
        this.router.navigate(['/clientes']);
        swal.fire(
          'Cliente guardado',
          `El cliente: ${cliente.nombre} ${cliente.apellido} ha sido creado con exito en la base de datos!`,
          'success'
        );
      },
      (error) => {
        this.errores = error.error.errores as string[];
        console.error('Codigo del error desde el backend: ' + error.status);
        console.error(error.error.errores);
        this.datosCorrectos();
      }
    );
  }

  update(): void {
    console.log(this.cliente);
    this.clienteService.update(this.cliente).subscribe(
      (respuesta) => {
        this.router.navigate(['/clientes']);
        swal.fire({
          title: 'Cliente actualizado',
          html: `<img src="https://3.bp.blogspot.com/-1qt4i33YzgQ/UAtwzw7XjLI/AAAAAAAAAKk/wb1QucDQmao/s320/todo+bien.png">
                <br>
                ${respuesta.Mensaje}: ${respuesta.Cliente.nombre} ${respuesta.Cliente.apellido}`,
          icon: 'success',
        });
      },
      (error) => {
        this.errores = error.error.errores as string[];
        console.error(
          'Codigo del error desde el backend: ' + error.error.status
        );
        console.error(error.error.errores);
        this.datosCorrectos();
      }
    );
  }

  campararRegion(o1: Region, o2: Region): boolean {
    return o1 === null || o2 === null || o1 === undefined || o2 === undefined
      ? false
      : o1.id == o2.id
      ? true
      : false;
  }
}
